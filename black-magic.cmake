# Copy the udev rules so black magic probe shows up with standard name
if (NOT EXISTS /etc/udev/rules.d/99-black-magic.rules)
	message("You may need to run with sudo first time to allow this copy")
	file(COPY ${CMAKE_CURRENT_LIST_DIR}/99-black-magic.rules DESTINATION /etc/udev/rules.d)
endif()

# Write flash
add_custom_target(flash-black-magic DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_OUTPUT}.elf COMMAND arm-none-eabi-gdb ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_OUTPUT}.elf -batch -x ${CMAKE_CURRENT_LIST_DIR}/commands/flash.txt)

# Create UART session
add_custom_target(uart-black-magic COMMAND screen /dev/ttyBMPUART 9600)